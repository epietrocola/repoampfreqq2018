GUI Kit licence. Commercial licence. 

The Commercial License grants you, the purchaser, an ongoing, non-exclusive, worldwide license to make use of the digital work, the Kit that you selected. 

1. You are licensed to use the Kit to create one Single End Product for yourself or for one client, the End Product may be sold.

2. You can modify, manipulate, copy, edit as a whole or part of the Kit and create a software item or other Single End Product. You can combine the Kit with other works and make a derivative work from it. The resulting works are subject to the terms of this license. The End Product may be sold. 

3. You can’t use the Item in any application allowing an end user to customise a digital or physical product to their specific needs, such as an “WYSIWYG(what you see is what you get)” “on demand”, “made to order” or “build it yourself” application. For this type of licence please contact us at info@vogerdesign.com

4.You can’t re-distribute the Kit or a part of it as stock, in a tool or template, or with source files. You can’t do this with an Kit either on its own or bundled with other items, and even if you modify the Kit. You can’t re-distribute or make available the Kit as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free. For this type of licence please contact us at info@vogerdesign.com

-

For other type of licence please contact us at info@vogerdesign.com
