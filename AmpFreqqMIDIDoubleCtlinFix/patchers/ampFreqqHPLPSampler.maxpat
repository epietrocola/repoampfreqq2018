{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 5,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 466.0, 153.0, 640.0, 480.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-4",
					"index" : 2,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 327.166687, 823.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-3",
					"index" : 1,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 282.333313, 823.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-2",
					"index" : 2,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 476.0, 35.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-1",
					"index" : 1,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 409.0, 35.0, 30.0, 30.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2316",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 55.0, 360.133301, 77.0, 22.0 ],
					"style" : "",
					"text" : "r filterMode4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2314",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 117.333328, 398.901367, 77.0, 22.0 ],
					"style" : "",
					"text" : "r filterMode5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2312",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 500.666656, 444.789063, 51.0, 22.0 ],
					"style" : "",
					"text" : "mode 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2310",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 55.0, 441.852539, 51.0, 22.0 ],
					"style" : "",
					"text" : "mode 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2307",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 117.333328, 441.852539, 51.0, 22.0 ],
					"style" : "",
					"text" : "mode 9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2305",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 522.0, 183.726563, 93.0, 22.0 ],
					"style" : "",
					"text" : "r freqPanInput2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2301",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.333344, 110.352539, 139.0, 22.0 ],
					"style" : "",
					"text" : "if $i1 == 0 then -1 else 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2300",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 442.333344, 80.0, 93.0, 22.0 ],
					"style" : "",
					"text" : "r freqPanInput1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2292",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 442.333344, 143.226563, 33.0, 22.0 ],
					"style" : "",
					"text" : "sig~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2293",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 463.676117, 183.726563, 45.0, 22.0 ],
					"style" : "",
					"text" : "pan2S"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2294",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 416.333344, 183.726563, 45.0, 22.0 ],
					"style" : "",
					"text" : "pan2S"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2295",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 468.333344, 221.831055, 33.0, 22.0 ],
					"style" : "",
					"text" : "sig~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2296",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 497.676117, 260.331055, 45.0, 22.0 ],
					"style" : "",
					"text" : "pan2S"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2297",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 442.333344, 260.331055, 45.0, 22.0 ],
					"style" : "",
					"text" : "pan2S"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2291",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 618.333374, 305.998535, 92.0, 22.0 ],
					"style" : "",
					"text" : "r freqFilterInput"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2290",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 252.333359, 328.598633, 92.0, 22.0 ],
					"style" : "",
					"text" : "r freqFilterInput"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2277",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 618.333374, 335.000977, 119.0, 22.0 ],
					"style" : "",
					"text" : "scale 16 135 135 16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2278",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 92.0, 135.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 84.628906, 33.0, 22.0 ],
									"style" : "",
									"text" : "float"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3676",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 122.0, 144.628906, 75.0, 22.0 ],
									"style" : "",
									"text" : "loadmess 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2689",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 62.0, 108.628906, 82.0, 22.0 ],
									"style" : "",
									"text" : "loadmess 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2682",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 144.628906, 49.0, 22.0 ],
									"style" : "",
									"text" : "pack f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2669",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 179.981445, 43.0, 22.0 ],
									"style" : "",
									"text" : "line 0."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2690",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2691",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 269.257813, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2691", 0 ],
									"source" : [ "obj-2669", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 0 ],
									"source" : [ "obj-2682", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 1 ],
									"order" : 1,
									"source" : [ "obj-2689", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2682", 1 ],
									"order" : 0,
									"source" : [ "obj-2689", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-2690", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 2 ],
									"source" : [ "obj-3676", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2682", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "Luca",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"selectioncolor" : [ 0.720698, 0.16723, 0.080014, 1.0 ],
									"textcolor_inverse" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
									"fontname" : [ "Open Sans Semibold" ],
									"accentcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
									"bgcolor" : [ 0.904179, 0.895477, 0.842975, 0.56 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
									"color" : [ 0.475135, 0.293895, 0.251069, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "dark-night-patch",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39
									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"patchlinecolor" : [ 0.439216, 0.74902, 0.254902, 0.898039 ],
									"accentcolor" : [ 0.952941, 0.564706, 0.098039, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 618.333374, 389.500977, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p newLerp"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-2279",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 596.333374, 695.289063, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2280",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 793.333374, 444.789063, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2281",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 713.333374, 444.789063, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2282",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 618.333374, 444.789063, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2283",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 793.333374, 480.289063, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2284",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 713.333374, 480.289063, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2285",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 623.333374, 480.289063, 57.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-2286",
					"linmarkers" : [ 0.0, 11025.0, 16537.5 ],
					"logmarkers" : [ 0.0, 100.0, 1000.0, 10000.0 ],
					"maxclass" : "filtergraph~",
					"nfilters" : 1,
					"numinlets" : 8,
					"numoutlets" : 7,
					"outlettype" : [ "list", "float", "float", "float", "float", "list", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 488.333344, 517.852539, 360.0, 155.0 ],
					"setfilter" : [ 0, 1, 1, 0, 0, 19912.126953, 1.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"style" : "",
					"varname" : "filtergraph~[4]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-2287",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 476.0, 695.289063, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2288",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 618.333374, 362.375, 47.333336, 22.0 ],
					"style" : "",
					"text" : "mtof"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-2266",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 247.333359, 699.901367, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2267",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 422.333344, 449.401367, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2268",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 342.333344, 449.401367, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"hidden" : 1,
					"id" : "obj-2269",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 247.333359, 449.401367, 48.0, 23.0 ],
					"style" : "",
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2270",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 422.333344, 484.901367, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2271",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 342.333344, 484.901367, 55.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-2272",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 252.333359, 484.901367, 57.0, 23.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"id" : "obj-2273",
					"linmarkers" : [ 0.0, 11025.0, 16537.5 ],
					"logmarkers" : [ 0.0, 100.0, 1000.0, 10000.0 ],
					"maxclass" : "filtergraph~",
					"nfilters" : 1,
					"numinlets" : 8,
					"numoutlets" : 7,
					"outlettype" : [ "list", "float", "float", "float", "float", "list", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 117.333328, 523.464844, 360.0, 155.0 ],
					"setfilter" : [ 0, 2, 1, 0, 0, 20.601723, 1.0, 3.02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ],
					"style" : "",
					"varname" : "filtergraph~[3]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-2274",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 117.333328, 699.901367, 92.0, 23.0 ],
					"style" : "",
					"text" : "biquad~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2275",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 7,
							"minor" : 3,
							"revision" : 5,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"rect" : [ 92.0, 135.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "float" ],
									"patching_rect" : [ 50.0, 84.628906, 33.0, 22.0 ],
									"style" : "",
									"text" : "float"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3676",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 122.0, 144.628906, 75.0, 22.0 ],
									"style" : "",
									"text" : "loadmess 0."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2689",
									"maxclass" : "newobj",
									"numinlets" : 1,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 62.0, 108.628906, 82.0, 22.0 ],
									"style" : "",
									"text" : "loadmess 20."
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2682",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 144.628906, 49.0, 22.0 ],
									"style" : "",
									"text" : "pack f f"
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 12.0,
									"id" : "obj-2669",
									"maxclass" : "newobj",
									"numinlets" : 3,
									"numoutlets" : 2,
									"outlettype" : [ "", "" ],
									"patching_rect" : [ 50.0, 179.981445, 43.0, 22.0 ],
									"style" : "",
									"text" : "line 0."
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2690",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 40.0, 30.0, 30.0 ],
									"style" : ""
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2691",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 50.0, 269.257813, 30.0, 30.0 ],
									"style" : ""
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-2691", 0 ],
									"source" : [ "obj-2669", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 0 ],
									"source" : [ "obj-2682", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 1 ],
									"order" : 1,
									"source" : [ "obj-2689", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2682", 1 ],
									"order" : 0,
									"source" : [ "obj-2689", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-2690", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2669", 2 ],
									"source" : [ "obj-3676", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2682", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "Luca",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
										"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}
,
									"selectioncolor" : [ 0.720698, 0.16723, 0.080014, 1.0 ],
									"textcolor_inverse" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
									"fontname" : [ "Open Sans Semibold" ],
									"accentcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
									"bgcolor" : [ 0.904179, 0.895477, 0.842975, 0.56 ],
									"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
									"color" : [ 0.475135, 0.293895, 0.251069, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "dark-night-patch",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "gradient",
										"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39
									}
,
									"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
									"patchlinecolor" : [ 0.439216, 0.74902, 0.254902, 0.898039 ],
									"accentcolor" : [ 0.952941, 0.564706, 0.098039, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 252.333359, 398.901367, 67.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"style" : "",
						"tags" : ""
					}
,
					"style" : "",
					"text" : "p newLerp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2276",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 252.333359, 360.133301, 47.333336, 22.0 ],
					"style" : "",
					"text" : "mtof"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2294", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2293", 0 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-2266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2270", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2271", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2268", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2272", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2273", 7 ],
					"source" : [ "obj-2270", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2273", 6 ],
					"source" : [ "obj-2271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2273", 5 ],
					"source" : [ "obj-2272", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2266", 0 ],
					"order" : 0,
					"source" : [ "obj-2273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2267", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2273", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2268", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2273", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2269", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2273", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2274", 0 ],
					"order" : 1,
					"source" : [ "obj-2273", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-2274", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2272", 0 ],
					"source" : [ "obj-2275", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2275", 0 ],
					"source" : [ "obj-2276", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2288", 0 ],
					"source" : [ "obj-2277", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2285", 0 ],
					"source" : [ "obj-2278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-2279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2283", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2280", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2284", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2285", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2286", 7 ],
					"source" : [ "obj-2283", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2286", 6 ],
					"source" : [ "obj-2284", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2286", 5 ],
					"source" : [ "obj-2285", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2279", 0 ],
					"order" : 0,
					"source" : [ "obj-2286", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2280", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2286", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2281", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2286", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2282", 0 ],
					"hidden" : 1,
					"source" : [ "obj-2286", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2287", 0 ],
					"order" : 1,
					"source" : [ "obj-2286", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-2287", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2278", 0 ],
					"source" : [ "obj-2288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2276", 0 ],
					"source" : [ "obj-2290", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2277", 0 ],
					"source" : [ "obj-2291", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2293", 1 ],
					"order" : 0,
					"source" : [ "obj-2292", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2294", 1 ],
					"order" : 1,
					"source" : [ "obj-2292", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2296", 0 ],
					"source" : [ "obj-2293", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-2293", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2297", 0 ],
					"source" : [ "obj-2294", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-2294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2296", 1 ],
					"order" : 0,
					"source" : [ "obj-2295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2297", 1 ],
					"order" : 1,
					"source" : [ "obj-2295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2266", 0 ],
					"source" : [ "obj-2296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2279", 0 ],
					"source" : [ "obj-2296", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2274", 0 ],
					"source" : [ "obj-2297", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2287", 0 ],
					"source" : [ "obj-2297", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2301", 0 ],
					"source" : [ "obj-2300", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2292", 0 ],
					"source" : [ "obj-2301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2295", 0 ],
					"source" : [ "obj-2305", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2273", 0 ],
					"order" : 1,
					"source" : [ "obj-2307", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2286", 0 ],
					"order" : 0,
					"source" : [ "obj-2307", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2273", 0 ],
					"source" : [ "obj-2310", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2286", 0 ],
					"source" : [ "obj-2312", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2307", 0 ],
					"source" : [ "obj-2314", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2310", 0 ],
					"order" : 1,
					"source" : [ "obj-2316", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2312", 0 ],
					"order" : 0,
					"source" : [ "obj-2316", 0 ]
				}

			}
 ],
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "Luca",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color1" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
						"color2" : [ 0.65098, 0.666667, 0.662745, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}
,
					"selectioncolor" : [ 0.720698, 0.16723, 0.080014, 1.0 ],
					"textcolor_inverse" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"fontname" : [ "Open Sans Semibold" ],
					"accentcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
					"bgcolor" : [ 0.904179, 0.895477, 0.842975, 0.56 ],
					"elementcolor" : [ 0.786675, 0.801885, 0.845022, 1.0 ],
					"color" : [ 0.475135, 0.293895, 0.251069, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "dark-night-patch",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "gradient",
						"color1" : [ 0.376471, 0.384314, 0.4, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39
					}
,
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"patchlinecolor" : [ 0.439216, 0.74902, 0.254902, 0.898039 ],
					"accentcolor" : [ 0.952941, 0.564706, 0.098039, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
